from django.apps import AppConfig


class AbutAppConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'abut_app'
